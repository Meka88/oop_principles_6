package memory_management;

import class_vs_object.Animal;
import class_vs_object.House;

import javax.xml.ws.Holder;

public class UnderstandingMemory {
    public static void main(String[] args) {
        /*
        Data types:
        1. Primitives: byte, short, int, long, float, double, char, boolean
        2. Reference over 5000

         */
        int age = 45;

        Animal a = new Animal(); // does not have toString method
        House h = new House(); // has toString() method

        System.out.println(a); // you get location
        System.out.println(h); // you get a value
    }
}
