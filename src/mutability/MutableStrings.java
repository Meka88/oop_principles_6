package mutability;

public class MutableStrings {
    public static void main(String[] args) {
        /*
        There are two classes used for mutable Strings:
        1. String Builder
        2. String Buffer
        two the same except StringBuffer is thread-safe(synchronized)
        StringBuffer is slower compared to StringBuilder
         */

        StringBuilder sBuilder = new StringBuilder("Chicago");
        StringBuffer sBuffer = new StringBuffer("Miami");

        sBuilder.insert(3, "Hello");

        System.out.println(sBuilder);
        System.out.println(sBuilder.reverse());
    }
}
