package mutability;

public class Exercise02_InjectStar {
    public static String injectStar(String str){

        if(str.length() <= 1) return "";
        else if(str.length() % 2 == 0){
            String firstHalf = str.substring(0, str.length()/2);
            String secondHalf = str.substring(str.length()/2);
            return firstHalf + "*" + secondHalf;
        }
        else{
            String firstPart = str.substring(0, str.length()/2);
            String lastPart = str.substring(str.length()/2 + 1);
            char middle = str.charAt(str.length()/2);

            return firstPart + "*" + middle + "*" + lastPart;
        }
    }

    public static void main(String[] args) {
        System.out.println(injectStar("java"));
        System.out.println(injectStar("Meerim"));
        System.out.println(injectStar("u"));
    }
}
