package mutability;

public class Exercise01_Reverse {
    public static void main(String[] args) {
        System.out.println(reverseString("Java"));

        System.out.println(isPalindrome("madam"));
        System.out.println(isPalindrome("Civic"));
    }

    public static String reverseString(String str){
//        StringBuilder str1 = new StringBuilder(str);
//        str1.reverse();
        return new StringBuilder(str).reverse().toString();
    }

    public static boolean isPalindrome(String str) {
        return str.equals(new StringBuilder(str).reverse().toString());
    }
}
