package class_vs_object;

public class Student_Practice {
    public static void main(String[] args) {
        Student student1 = new Student();

        System.out.println(Student.course); // made a static so we can access with class

        //Student.course = "Java developer"; can't change because it's final
        System.out.println(Student.course);

        System.out.println(Student.totalNumberOfStudents);
        Student.decreaseNumberOfStudentsByOne();
        Student.decreaseNumberOfStudents(3);

        System.out.println(Student.totalNumberOfStudents);
    }
}
