package class_vs_object;

public class Animal {

    {
        System.out.println("INSTANCE BLOCK IS RUNNING");
        // this is an instance block is used to run block of code for each object being created
        // will be last one to run where object is created and for each of the object
        // runs top to bottom
    }


    static{
        System.out.println("STATIC BLOCK IS RUNNING");
        // static block is used to run block of code even before main method or other code
        // only runs once and first one at the top
    }


    @Override
    protected void finalize() {
        System.out.println("A Garbage collection happened here!!!");
    }


    public static void main(String[] args) {
        Animal a1;
        System.out.println("Hello world!");

        a1 = new Animal();
    }
}
