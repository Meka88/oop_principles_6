package class_vs_object;

public class Fruits {
    public final boolean hasColor = true; // can't be changed after declared final
    public String name;
    public String taste;


    public static void main(String[] args) {
        Fruits fruit1 = new Fruits();

        System.out.println(fruit1.hasColor);
    }
}
