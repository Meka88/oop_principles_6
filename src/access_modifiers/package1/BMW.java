package access_modifiers.package1;

class BMW { // accessible only from the same package
    public BMW(){
        // default constructor
    }



    public static void main(String[] args) {
        BMW b1 = new BMW();
    }
}
