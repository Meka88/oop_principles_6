package oop_principles.abstraction;

public class Iphone extends Phone implements WiFi, Camera, Bluetooth{
    @Override
    public void call() {
        System.out.println("iPhone calls");
    }

    @Override
    public void text() {
        System.out.println("iPhone texts");
    }

    @Override
    public void ring() {
        System.out.println("iPhone rings");
    }

    @Override
    public void connectWifi() {
        System.out.println("Iphone connect to WiFi");
    }

    @Override
    public void takePhoto() {
        System.out.println("Iphone takes photo");
    }

    @Override
    public void recordVideo() {
        System.out.println("Iphone records video");
    }

    @Override
    public void connectBluetooth() {
        System.out.println("Iphone connects to Bluetooth");
    }
}
